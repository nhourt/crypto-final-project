//Encrypt a .card file.

#include <qca.h>
#include <QFile>
#include <stdio.h>
#include <QString>



void make_atm_keys(char * name, char * key) { //Generates RSA 4096 bit keypair. PrivKey at ATM, pubkey at Bank.
    QCA::PrivateKey privateKey = QCA::KeyGenerator().createRSA(4096);
    if (privateKey.isNull()) {
        printf("Unable to create private key.\n");
        return;
    }

    QCA::PublicKey publicKey = privateKey.toPublicKey();
    if (publicKey.isNull()) {
        printf("Unable to create public key.\n");
        return;
    }

    publicKey.toPEMFile(QString(name).append(".pubKey")); //Write the Public Key
    privateKey.toPEMFile(QString(name).append(".privKey"),QCA::SecureArray(key));

    return;
}

void make_bank_keys(char * key) { //Generates RSA 4096 bit KeyPair. Pubkey distributed to ATMs, Privkey stays with Bank.

    QCA::PrivateKey privateKey = QCA::KeyGenerator().createRSA(4096);
    if (privateKey.isNull()) {
        printf("Unable to create private key.\n");
        return;
    }

    QCA::PublicKey publicKey = privateKey.toPublicKey();
    if (publicKey.isNull()) {
        printf("Unable to create public key.\n");
        return;
    }

    publicKey.toPEMFile(QString("bank.pubKey")); //Write the Public Key
    privateKey.toPEMFile(QString("bank.privKey"),QCA::SecureArray(key));

    return;
}

void create_user_key(char * username, char * PIN) {
    /*
    Create a user card key-pair. private key encrypted with a random 32 Byte value.
    This value is encrypted with AES256 with the PIN as a key.
    Files Written:
    <Client>.pubkey = The Public Key given to the Bank.
    <Client>.card = PEM with passphrase == random 32 Byte Value.
    <Client>.pass> = Random 32 Byte passphrase for <client>.card encrypted with AES 256 using key SHA256( PIN ).
        <32 Bytes> -- Initialization Vector used for AES
        <Rest of File> -- the 32 Byte Passphrase encrypted with AES
    */

    //First, generate the RSA keypair.

    QCA::PrivateKey privateKey = QCA::KeyGenerator().createRSA(4096);
    if (privateKey.isNull()) {
        printf("Unable to create private key.\n");
        return;
    }

    QCA::PublicKey publicKey = privateKey.toPublicKey();
    if (publicKey.isNull()) {
        printf("Unable to create public key.\n");
        return;
    }
    //Write the Public Key to a file. This is unencrypted.
    publicKey.toPEMFile(QString().sprintf("%s.pubKey", username));

    //Setup Everything for AES 256.
    QCA::InitializationVector iv(32); //The IV
    QCA::SymmetricKey key(32); //The key to decrypt <client>.card
    QCA::SecureArray pin(PIN);
    QCA::SymmetricKey aeskey(QCA::Hash("sha256").hash(pin)); //The key to encrypt <client>.pass

    //Write the private key PEM to disk.
    privateKey.toPEMFile(QString().sprintf("%s.card", username), key);

    //Create the file which stores the key we just encrypted users private PEM with.

    QCA::Cipher aes(QString("aes256"),QCA::Cipher::CBC,
                     // use Default padding, which is equivalent to PKCS7 for CBC
                     QCA::Cipher::DefaultPadding,
                     // this object will encrypt
                     QCA::Encode,
                     aeskey, iv);

    QCA::SecureArray enckey(key.toByteArray());
    QCA::SecureArray enc = aes.process(enckey);

    QByteArray data(enc.toByteArray());
    data.prepend(iv.toByteArray());

    QFile pass(QString(username).append(".pass"));
    if(!pass.open(QIODevice::WriteOnly)) {
        printf("Error opening file.\n");
        return;
    }
    pass.write(data);
    pass.close();

}

int main(int argc, char *argv[])
{
    QCA::Initializer init;

    if(argc < 2) {
        printf("Usage: ./KeyMaster -[a,b,u] <atm,bank,user> \n");
        return -1;
    }

    if(!strcmp(argv[1],"-u")) {
        if (argc != 4) {
            printf("Usage: ./KeyMaster -u <username> <pin>\n");
            return -1;
        }

        else {
            create_user_key(argv[2],argv[3]);
        }
    }
    else if(!strcmp(argv[1],"-b")) {
        if (argc != 3) {
            printf("Usage: ./KeyMaster -b <passphrase>\n");
            return -1;
        }
        else {
            make_bank_keys(argv[2]);
        }
    }
    else if(!strcmp(argv[1],"-a")) {
        if (argc != 4) {
            printf("Usage: ./KeyMaster -a <atm-name> <passphrase>\n");
            return -1;
        }
        else {
            make_atm_keys(argv[2],argv[3]);
        }
    }
    else {
        printf("Usage: ./KeyMaster -[a,b,u] <atm,bank,user> \n");
        return -1;
    }
    return 0;
}



/*
//----------------------------------------------------------------------------------------
//      Decrypt Testing
//----------------------------------------------------------------------------------------


    QFile dec(QString(argv[1]).append(".card"));
    dec.open(QIODevice::ReadOnly);
    QByteArray encdata = dec.readAll();

    QByteArray newiv;
    for(int i =0;i<16;i++) { //copy I-Vector
        newiv[i] = encdata[i];
    }
    QByteArray ciphertext;
    for(int i =16;i < encdata.size();i++) {// Copy Ciphertext
        ciphertext[i-16] = encdata[i];
    }

    //Some Debug Output because why not?
    printf("CipherText from File: %s\n",qPrintable(QCA::arrayToHex(ciphertext)));
    printf("Initialiation Vector From File %s\n",qPrintable(QCA::arrayToHex(newiv)));

    QCA::InitializationVector niv(newiv);


    QCA::SecureArray newkey(argv[2]);
    newkey = QCA::Hash("sha256").hash(newkey);

    QCA::SymmetricKey newseckey(newkey);

    QCA::Cipher decipher(QString("aes256"),QCA::Cipher::CBC,
                               // use Default padding, which is equivalent to PKCS7 for CBC
                               QCA::Cipher::DefaultPadding,
                               // this object will encrypt
                               QCA::Decode,
                               newseckey, niv);


    QCA::SecureArray deciphertext = decipher.process(enc);

    if(!decipher.ok()) {
        printf("Decipher Failed\n");
    }
    //deciphertext = decipher.final();
    printf("Decrypted .card file contents: \n %s\n",deciphertext.data());

}
*/
