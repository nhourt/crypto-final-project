#ifndef ATMCONTROLLER_H
#define ATMCONTROLLER_H

#include <QHash>
#include <QObject>
#include <QNoDebug>
#include <QtCrypto>
#include <QTcpSocket>
#include <QTextStream>

class AtmController : public QObject
{
    Q_OBJECT
public:
    explicit AtmController(QObject *parent = 0);
signals:

public slots:
    void connectToProxy();

protected slots:
    void userPrompt();
    void queuePrompt();
    void socketReadable();
    void socketError();

protected:
    enum AtmState {StateIdle,
                   StateLoggingIn,
                   StateAuthenticated,
                   StateAskingBalance,
                   StatePendingWithdrawal,
                   StatePendingTransfer,
                   StateBricked
                  };
    AtmState currentState;
    QString authenticatedUser;
    QString name;
    QCA::SecureArray lastNonce;

    QTcpSocket bankSocket;

    QTextStream qin;
    QTextStream qout;

    QHash<QString,QDateTime> blockedUntil;
    QHash<QString,int> failedLogins;

    QCA::PrivateKey atmKey;
    QCA::PrivateKey userKey;
    QCA::PublicKey bankKey;
    QString userName;

    QCA::Cipher aes;
    QCA::SymmetricKey sessionKey;
    QCA::InitializationVector sessionIV;

    const static int IV_SIZE = 32;
    const static int RSA_CIPHERTEXT_SIZE = 512;
    //We leave one byte for the cipher's padding
    const static int PACKET_SIZE = 1023;

    void processUserCommand(QString command);

    void markFailedLogin(QString username);
    bool userBruteBlocked(QString username);
    QCA::SecureArray getPin();
    bool loadAtmCard(QCA::SecureArray pin, QCA::SecureArray cardKeyPackage);

    void beginRemoteAuthentication();
    void finishRemoteAuthentication(QByteArray crypticResponse);

    void askBalance();
    void reportBalance(QByteArray response);

    void startWithdrawal(QString amount);
    void completeWithdrawal(QByteArray response);

    void startTransfer(QString amount, QString username);
    void completeTransfer(QByteArray response);

    void sendMessageToBank(QByteArray message);

    QCA::SecureArray encipher(QByteArray message);
    QByteArray decipher(QByteArray message);
    void decodePacket(QByteArray &packet);

    bool validNonce(QByteArray suspectNonce);
    QCA::SecureArray getNonce();

    void tamperingDetected();

    void doThePhoenixThang();
};

#endif // ATMCONTROLLER_H
