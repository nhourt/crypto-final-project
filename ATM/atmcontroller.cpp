#include "atmcontroller.h"
#include "money.h"

#include <QCoreApplication>
#include <QStringList>
#include <QFile>
#include <qca.h>
#include <cstdio>
#include <QDateTime>
#include <QFileInfo>
#include <QTimer>

#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KPIN  "\x1B[131m"
#define KNRM  "\x1B[0m"
#define KWOW  "\x1b[5m"
#define KWAO  "\x1b[25m"
#define KGRN  "\x1b[32m"

AtmController::AtmController(QObject *parent) :
    QObject(parent),
    currentState(StateIdle),
    qin(stdin, QIODevice::ReadOnly),
    qout(stdout, QIODevice::WriteOnly),
    aes("aes256", QCA::Cipher::CBC)
{
    //Clear terminal
    qout << "\E[H\E[2J";

    connect(&bankSocket, SIGNAL(connected()), this, SLOT(userPrompt()));
    connect(&bankSocket, SIGNAL(readyRead()), this, SLOT(socketReadable()));
    connect(&bankSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(socketError()));

    QCA::ConvertResult result;
    bankKey = QCA::PublicKey::fromPEMFile("bank.pubKey", &result);
    if (result != QCA::ConvertGood) {
        qWarning() << "This ATM has not been configured properly. Error ID" << result;
        currentState = StateBricked;
    }

    //Get the name of the ATM.
    QFile config("./atm.config");
    if(!config.open(QIODevice::ReadOnly)) {
        printf("~Wow~ Why you mess with my configs! :< \n");
        printf("Not even mad, everything will just fail now\n.");
        currentState = StateBricked;
    }
    QString data = QString(config.readAll());
    QStringList confdat = data.split('\n');
    QCA::SecureArray atmpemkey(confdat[1].toAscii());
    name = confdat[0];
    config.close();


    //Get the ATM's Private Key
    QCA::ConvertResult privresult;
    atmKey = QCA::PrivateKey::fromPEMFile(QString(name).append(".privKey"),atmpemkey, &privresult);
    if (privresult != QCA::ConvertGood) {
        qWarning() << "This ATM has not been configured properly.";
        currentState = StateBricked;
    }
    //If we got this far, we're all set!
}

void AtmController::connectToProxy()
{
    qDebug() << "Attempting to connect to proxy...";
    bankSocket.connectToHost("localhost", qApp->arguments()[1].toInt());
}

void AtmController::processUserCommand(QString command)
{
    QStringList commandList = command.split(" ", QString::SkipEmptyParts);
    if(commandList[0] == "logout") {
        qout << "Goodbye." << endl;
        doThePhoenixThang();
        return;
    }

    if(commandList[0] == "login" && commandList.size() == 2) {
        //LOGIN
        if (currentState == StateIdle)
        {
            userName = commandList[1];
            beginRemoteAuthentication();
        } else {
            qWarning() << "You are already logged in." << endl;
            queuePrompt();
            return;
        }
    } else if (commandList[0] == "balance" && commandList.size() == 1) {
        //BALANCE
        if (currentState != StateAuthenticated) {
            qWarning() << "You need to login first!";
            queuePrompt();
            return;
        }
        else
            askBalance();
    } else if (commandList[0] == "withdraw" && commandList.size() == 2) {
        //WITHDRAW
        if (currentState != StateAuthenticated) {
            qWarning() << "You need to login first!";
            queuePrompt();
            return;
        }
        else
            startWithdrawal(commandList[1]);
    } else if (commandList[0] == "transfer" && commandList.size() == 3) {
        //TRANSFER
        if (currentState != StateAuthenticated)
            qWarning() << "You need to login first!\n";
        else
            startTransfer(commandList[1],commandList[2]);
    } else {
        qWarning() << "Unknown or not-well-formed command:" << commandList.join(" ");
        queuePrompt();
        return;
    }
}

void AtmController::userPrompt()
{
    if (currentState == StateBricked) {
        if (bankKey.isNull() || name.length() == 0) {
            //We're *really* bricked. ATM is non-functional, and should just exit.
            qApp->exit(1);
            return;
        }
        qWarning() << "The ATM has encountered a fatal error and must restart. Please contact administrator.";
        doThePhoenixThang();
        return;
    }

    //Sadly, there's no easy way to do event-driven terminal I/O
    QString command;

    while (command.isEmpty()) {
        if (currentState == StateIdle)
            qout << "atm> " << flush;
        else
            qout << "atm[" << authenticatedUser << "]> " << flush;

        command = qin.readLine();
    }

    processUserCommand(command);
}
void AtmController::queuePrompt()
{
    QMetaObject::invokeMethod(this, "userPrompt", Qt::QueuedConnection);
}

void AtmController::socketReadable()
{
    QByteArray packet = bankSocket.readAll();

    //No matter how we leave this function, a prompt must be queued. This object ensures that happens.
    QObject promptCanary;
    connect(&promptCanary, SIGNAL(destroyed()), this, SLOT(queuePrompt()));

    //When logging in, the session key has not been established yet.
    if (currentState != StateLoggingIn) {
        decodePacket(packet);
        qDebug() << "Packet in from bank:" << packet;

        if (!validNonce(QByteArray::fromHex(packet.split(' ')[0]))) {
            qDebug() << "Invalid nonce! Bugging out!";
            tamperingDetected();
            return;
        }
    }

    //The interpretation of a packet is a function of our state, not the packet contents.
    switch(currentState) {
    case StateIdle:
        qDebug() << "lolwut? Unexpected packet in from bank.";
        break;
    case StateBricked:
        //We're burned. Do nothing, let the ATM exit.
        break;
    case StateLoggingIn:
        finishRemoteAuthentication(packet);
        break;
    case StateAskingBalance:
        reportBalance(packet);
        break;
    case StatePendingWithdrawal:
        completeWithdrawal(packet);
        break;
    case StatePendingTransfer:
        completeTransfer(packet);
        break;
    case StateAuthenticated:
        //Note that StateAuthenticated is basically StateIdle, except that the user is logged in.
        qDebug() << "Spurious packet! I'm done.";
        tamperingDetected();
        break;
    }
}

void AtmController::socketError()
{
    qWarning() << "Socket error occurred:" << bankSocket.errorString();
    currentState = StateBricked;
    queuePrompt();
}

bool AtmController::userBruteBlocked(QString username)
{
    QDateTime currentTime(QDateTime::currentDateTime());
    //Check if the current user is timed out.
    if(blockedUntil.contains(username)) {
        if (currentTime > blockedUntil[username]) {
            blockedUntil.remove(username);
        } else {
            qWarning("Too many failed login attempts. Please wait before logging in again.\n");
            return true;
        }
    }

    return false;
}

QCA::SecureArray AtmController::getPin()
{
    QCA::ConsolePrompt pin_input;
    pin_input.getHidden("PIN");
    pin_input.waitForFinished();
    QCA::SecureArray pin = pin_input.result();


    while (pin.size() != 5) {
        qWarning("%s%s~wow~ such invalid.\n\n\t~wow~\n\t\t%s such hax~\n%s%s\n\nPlease enter 5-character PIN.\n",KWOW,KMAG,KCYN,KWAO,KNRM);
        pin_input.getHidden("PIN");
        pin_input.waitForFinished();
        pin = pin_input.result();
    }

    return pin;
}

void AtmController::markFailedLogin(QString username)
{
    if (failedLogins.contains(username)) {
        failedLogins[username]++;

        if (failedLogins[username] >= 5) {
            blockedUntil[username] = QDateTime::currentDateTime().addSecs(300);
            failedLogins.remove(username);
        }
    } else {
        failedLogins[username] = 1;
    }
}

bool AtmController::loadAtmCard(QCA::SecureArray pin, QCA::SecureArray cardKeyPackage)
{
    //Check if ATM card exists.
    QString cardFile = QString("user_cards/%1.card").arg(userName);
    if (!QFileInfo(cardFile).exists()) {
        qWarning() << "No such user or ATM card not present.";
        return false;
    }

    //Try to decrypt the .pass file from bank response.
    QCA::SymmetricKey pinHash(QCA::Hash("sha256").hash(pin));

    //Get IV from bank response.
    QCA::InitializationVector iv(cardKeyPackage.toByteArray().left(IV_SIZE));

    //Get the passphrase to decrypt.
    QByteArray passPhrase = cardKeyPackage.toByteArray().mid(IV_SIZE);

    QCA::Cipher aes(QString("aes256"),QCA::Cipher::CBC,
                     // use Default padding, which is equivalent to PKCS7 for CBC
                     QCA::Cipher::DefaultPadding,
                     // this object will encrypt
                     QCA::Decode,
                     pinHash, iv);

    QCA::SecureArray cardKey = aes.process(passPhrase);
    if(!aes.ok()) {
        qWarning() << "Invalid user or PIN.";
        markFailedLogin(userName);
        return false;
    }
    QCA::ConvertResult successful = QCA::ErrorDecode;
    userKey = QCA::PrivateKey::fromPEMFile(cardFile, cardKey, &successful);
    if (successful != QCA::ConvertGood) {
        qWarning() << "Invalid user or PIN. Error ID" << successful;
        markFailedLogin(userName);
        return false;
    }

    return true;
}

void AtmController::beginRemoteAuthentication()
{
    QByteArray message;

    //Packet format:
    // RSA_Bu(<atm name> <username> <timestamp> hex(<nonce n0>)) RSASig_Ar(<ciphertext>)

    //First, generate a nonce.
    lastNonce = QCA::InitializationVector(IV_SIZE);

    if(!atmKey.canSign()) {
        qDebug() << "Whoops. Can't sign with this key...";
        qWarning() << "This ATM has not been configured properly.";
        currentState = StateBricked;
        return;
    }
    if(atmKey.isNull()) {
        printf("Null Private Key\n");
        currentState = StateBricked;
        return;
    }

    //At this point, everything should be ready to go. Put it all in message, wrap it with Bank Pub Key, and send.
    QTextStream (&message) << name << " " << userName << " " << QDateTime::currentDateTime().toTime_t() << " " << lastNonce.toByteArray().toHex();
    qDebug() << "Message: " << message << endl;
    message = bankKey.encrypt(message,QCA::EME_PKCS1_OAEP).toByteArray();
    message.append(atmKey.signMessage(message, QCA::EMSA3_SHA1));
    qDebug() << "Packet Length: " << message.size();
    currentState = StateLoggingIn;

    bankSocket.write(message);
}

void AtmController::finishRemoteAuthentication(QByteArray crypticResponse)
{
    qDebug() << "Finishing Remote Auth";

    //Packet format:
    // RSA_Au(hex(AES_sha256(pin)(Cp)) hex(<nonce n1 = sha256(n0)>)) RSA_Cu(<session key> <session IV> <n1>)
    QByteArray sessionPackage = crypticResponse.right(RSA_CIPHERTEXT_SIZE);
    crypticResponse.truncate(RSA_CIPHERTEXT_SIZE);

    QCA::SecureArray bankresponse;
    atmKey.decrypt(crypticResponse, &bankresponse, QCA::EME_PKCS1_OAEP);
    QList<QByteArray> parsedResponse = bankresponse.toByteArray().split(' ');

    //First, some sanity checking and checking the nonce.
    if(parsedResponse.size() != 2 ||
            !validNonce(QByteArray::fromHex(parsedResponse[1]))) {
        tamperingDetected();
        return;
    }

    //We need to decrypt the locally stored user's private key, for this, we need to get a pin from the user.
    QCA::SecureArray pin = getPin();
    if(!loadAtmCard(pin, QByteArray::fromHex(parsedResponse[0]))) {
        currentState = StateBricked;
        return;
    }

    QCA::SecureArray package;
    if (!userKey.decrypt(sessionPackage, &package, QCA::EME_PKCS1_OAEP) ||
            package.toByteArray().right(IV_SIZE) != QByteArray::fromHex(parsedResponse[1])) {
        tamperingDetected();
        return;
    }
    qDebug() << "Got session package," << package.size() << "bytes.";

    qout << "Authenticated successfully." << endl;

    authenticatedUser = userName;
    userName = QString::null;
    sessionKey = package.toByteArray().left(IV_SIZE);
    sessionIV = package.toByteArray().mid(IV_SIZE, IV_SIZE);
    currentState = StateAuthenticated;
    failedLogins.remove(authenticatedUser);
}

void AtmController::askBalance() {
    QByteArray message;
    QCA::SecureArray nonce = getNonce();

    //Message format: <nonce> balance
    QTextStream (&message) << nonce.toByteArray().toHex() << " balance";

    currentState = StateAskingBalance;
    sendMessageToBank(message); //Encryption and networking handled here.
}

void AtmController::reportBalance(QByteArray response) {
    QList<QByteArray> fields = response.split(' ');
    currentState = StateAuthenticated;

    //Message format: <nonce> <balance>
    qout << "Balance: " << fields[1] << endl;
}

void AtmController::startWithdrawal(QString amount)
{
    if (!QRegExp("^\\d*(\\.\\d{2})?$").exactMatch(amount) || amount.toDouble() <= 0) {
        qWarning() << "Invalid withdrawal amount.";
        queuePrompt();
        return;
    }
    if (amount.contains('.'))
        amount.remove('.');
    else
        amount.append("00");

    QByteArray message;
    QCA::SecureArray nonce = getNonce();

    //Message format: <nonce> withdraw <amount>
    QTextStream(&message) << nonce.toByteArray().toHex() << " withdraw " << amount;

    currentState = StatePendingWithdrawal;
    sendMessageToBank(message);
}

void AtmController::completeWithdrawal(QByteArray response)
{
    QList<QByteArray> fields = response.split(' ');
    currentState = StateAuthenticated;

    //Message format: <nonce> <accept|reject> <balance>
    if (fields[1] == "reject") {
        qWarning() << "Sorry, insufficient funds to complete withdrawal.\nYour current balance is" << fields[2];
        return;
    }

    //qout << MONEY << endl << "Your balance is now " << fields[2] << endl;
    printf("%s %s %s",KGRN,MONEY,KNRM);
    qout << "Your balance is now " << fields[2] << endl;
}

void AtmController::startTransfer(QString amount, QString username)
{
    if (!QRegExp("^\\d*(\\.\\d{2})?$").exactMatch(amount) || amount.toDouble() <= 0) {
        qWarning() << "Invalid transfer amount.";
        queuePrompt();
        return;
    }
    if (amount.contains('.'))
        amount.remove('.');
    else
        amount.append("00");

    QByteArray message;
    QCA::SecureArray nonce = getNonce();

    //Message format: <nonce> transfer <amount> <username>
    QTextStream(&message) << nonce.toByteArray().toHex() << " transfer " << amount << " " << username;

    currentState = StatePendingTransfer;
    sendMessageToBank(message);
}

void AtmController::completeTransfer(QByteArray response)
{
    QList<QByteArray> fields = response.split(' ');
    currentState = StateAuthenticated;

    //Message format: <nonce> <accept|reject> <balance> <amounterror|usererror>
    if (fields[1] == "reject") {
        if (fields[3] == "amounterror") {
            qWarning() << "Transfer has failed due to insufficient funds. Your current balance is " << fields[2] << ".";
            qDebug() << "Insufficient funds to transfer.";
            return;
        }
        else if (fields[3] == "usererror") {
            qWarning() << "Transfer has failed. User does not exist.";
            qDebug() << "User does not exist.";
            return;
        }
    }

    qout << "Your balance is now " << fields[2] << endl;
}

void AtmController::sendMessageToBank(QByteArray message)
{
    qDebug() << "Sending" << message.size() << "byte message to bank:" << message;
    int rawSize = message.size();
    message.prepend((char*)&rawSize, sizeof(rawSize));
    if (message.size() < PACKET_SIZE)
        message.resize(PACKET_SIZE);

    bankSocket.write(encipher(message).toByteArray());
}

QCA::SecureArray AtmController::encipher(QByteArray message)
{
    if (currentState == StateIdle) {
        qDebug() << "Something's wrong... Trying to encipher before authenticated.";
        currentState = StateBricked;
        return QCA::SecureArray();
    }

    aes.setup(QCA::Encode, sessionKey, sessionIV);
    return aes.process(message);
}

QByteArray AtmController::decipher(QByteArray message)
{
    if (currentState == StateIdle) {
        qDebug() << "Something's wrong... Trying to decipher before authenticated.";
        currentState = StateBricked;
        return QByteArray();
    }

    aes.setup(QCA::Decode, sessionKey, sessionIV);
    QByteArray ret = aes.process(message).toByteArray();
    if (!aes.ok()) {
        qDebug() << "Failed to decrypt message.";
    }
    return ret;
}

void AtmController::decodePacket(QByteArray &packet)
{
    packet = decipher(packet);
    packet = packet.mid(sizeof(packet.size()), *(int*)packet.data());
}

bool AtmController::validNonce(QByteArray suspectNonce)
{
    if (QCA::Hash("sha256").hash(lastNonce).toByteArray()== suspectNonce) {
        lastNonce = QCA::Hash("sha256").hash(lastNonce).toByteArray();
        return true;
    }
    return false;
}

QCA::SecureArray AtmController::getNonce() {
    //Generate a new nonce from the lastNonce, store it, and return it.
    QCA::SecureArray newNonce = QCA::Hash("sha256").hash(lastNonce);
    lastNonce = newNonce;
    return newNonce;
}

void AtmController::tamperingDetected()
{
    qWarning() << "The ATM has detected an unauthorized party interfering with the session. For security reasons, this session will now end.";
    currentState = StateBricked;
}

///Just stop and think for a minute about what's going on right here...
void AtmController::doThePhoenixThang()
{
    AtmController *phoenix = new AtmController(qApp);
    //Preserve bruteforce records
    phoenix->blockedUntil = blockedUntil;
    phoenix->failedLogins = failedLogins;

    connect(this, SIGNAL(destroyed()), phoenix, SLOT(connectToProxy()));
    QTimer::singleShot(5000, this, SLOT(deleteLater()));
}
