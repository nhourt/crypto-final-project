/**
    @file atm.cpp
    @brief Top level ATM implementation file
 */

#include "atmcontroller.h"

#include <QCoreApplication>

int main(int argc, char* argv[])
{
    if(argc != 2)
    {
        printf("Usage: atm proxy-port\n");
        return -1;
    }

    QCoreApplication app(argc, argv);
    QCA::Initializer init;
    AtmController *ac = new AtmController;

    //Queue a call to ac.connectToProxy as soon as the event loop starts below
    QMetaObject::invokeMethod(ac, "connectToProxy", Qt::QueuedConnection);

    return app.exec();
}
