#-------------------------------------------------
#
# Project created by QtCreator 2013-11-21T23:17:16
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = ATM
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += crypto

TEMPLATE = app

QMAKE_CXXFLAGS_RELEASE += -DQT_NO_DEBUG_OUTPUT

SOURCES += main.cpp \
    atmcontroller.cpp

HEADERS += \
    atmcontroller.h \
    money.h
