#-------------------------------------------------
#
# Project created by QtCreator 2013-11-21T23:15:42
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = FinalProject
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = subdirs

SUBDIRS += \
    Bank \
    Proxy \
    ATM \
    KeyMaster
