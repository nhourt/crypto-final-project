#-------------------------------------------------
#
# Project created by QtCreator 2013-11-21T23:17:01
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = Proxy
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

QMAKE_CXXFLAGS += -fpermissive
