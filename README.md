This project is the submission for the final project in Cryptography and Network Security 1,
Fall 2013 at Rensselaer Polytechnic Institute.

The authors are Laura Babcock, Patrick Biernat and Nathan Hourt. Protocol documentation,
the submitted source code, and a virtual machine with the deployed system are available at
http://ge.tt/8LDdnZ61

The purpose of this project is to create a protocol by which multiple ATMs can communicate
securely over an untrusted network with a single central bank. A proxy is inserted between
the ATMs and the bank to enable an attacker to easily view or manipulate packets being sent.
The objective of the project is to craft the protocol such that an attacker capable of
viewing, injecting, altering, and/or dropping packets traveling between an ATM and the bank
is unable to launch any attack affecting bank customer balances. Denial of service attacks
are considered out of scope, as the attacker is granted full control over the network.

Note that we assume the physical ATMs are tamper resistant, meaning their keys cannot be
viewed or altered by an attacker; however, we also assume the attacker has read-only access
to the protocol description as well as our source code to implement it. We also assume that
the bank terminal is secured and only accessible to trusted bank employees.

This project is written in C++ and requires the Qt Framework version 4.8.x and the Qt
Cryptographic Architecture version 2.0.x using OpenSSL (other backends may work, but have
not been tested with this project).
