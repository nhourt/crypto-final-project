#-------------------------------------------------
#
# Project created by QtCreator 2013-11-21T23:16:43
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = Bank
CONFIG   += console crypto
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    consoleinterface.cpp \
    bankcontroller.cpp \
    atminterface.cpp

QMAKE_CXXFLAGS += -fpermissive
QMAKE_CXXFLAGS_RELEASE += -DQT_NO_DEBUG_OUTPUT

HEADERS += \
    consoleinterface.h \
    bankcontroller.h \
    atminterface.h
