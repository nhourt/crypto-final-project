/**
    @file bank.cpp
    @brief Top level bank implementation file
 */
#include "bankcontroller.h"
#include "consoleinterface.h"

#include <QtConcurrentRun>
#include <QCoreApplication>

int main(int argc, char* argv[])
{
    if(argc != 2)
    {
        printf("Usage: bank listen-port\n");
        return -1;
    }

    QCoreApplication app(argc, argv);
    QCA::Initializer qca;
    BankController *bank = BankController::globalInstance();
    Q_UNUSED(bank);
    ConsoleInterface cli;

    //The CLI blocks on stdin, so we need to put him in a separate thread.
    QThreadPool::globalInstance()->start(&cli);

    return app.exec();
}
