#ifndef BANKCONTROLLER_H
#define BANKCONTROLLER_H

#include <QMutex>
#include <QObject>
#include <QtCrypto>
#include <QNoDebug>
#include <QTcpServer>
#include <QCoreApplication>

/**
 * @brief The BankController class is the central point of activity for all bank activities such as checking
 * and updating balances, transferring funds, and also delegating AtmControllers to handle incoming ATM
 * connections.
 *
 * This is a singleton class. Retrieve the instance via the globalInstance method.
 */
class BankController : public QObject
{
    Q_OBJECT
private:
    explicit BankController(QObject *parent = 0);
    static BankController *instance;
    static QMutex instanceMutex;

public:
    static BankController *globalInstance()
    {
        instanceMutex.lock();
        if (instance == NULL)
            instance = new BankController(qApp);
        instanceMutex.unlock();

        return instance;
    }

    /**
     * @brief Creates a printable string of the balance provided
     * @param balance The balance to format all pretty like
     * @return The nicely formatted balance
     *
     * Formats balances nicely for printing, for example:
     * 0 becomes $0.00
     * 5 becomes $0.05
     * 5000 becomes $50.00
     */
    static QString printableBalance(QCA::BigInteger balance) {
        if (balance == 0)
            return "$0.00";

        QString stringBalance = balance.toString();

        if (stringBalance.length() == 1)
            stringBalance.prepend("0.0");
        else
            stringBalance.insert(-2, '.');

        return stringBalance.prepend('$');
    }

    /**
     * @brief Queries if the named client exists in the bank
     * @param clientName The case-sensitive name of the client to look for
     * @return true if the client exists; false otherwise
     */
    Q_INVOKABLE bool clientExists(QString clientName);

    /**
     * @brief Attempt to lock the client
     * @param clientName Case-sensitive name of the client to lock
     * @return true if the client could be locked; false otherwise
     *
     * This function and its partner unlockClient() are used to prevent a client from logging
     * in from two ATMs at once, or trying to update his balance from the bank while logged
     * in at an ATM. A client must be locked when he logs in at an ATM or before a change is
     * made to his account from the bank, and unlocked when the session or change is complete.
     */
    Q_INVOKABLE bool lockClient(QString clientName);

    /**
     * @brief Unlock the client
     * @param clientName Case-sensitive name of the client to unlock
     *
     * See description of lockClient().
     */
    Q_INVOKABLE void unlockClient(QString clientName);

    /**
     * @brief Queries the specified client's balance and returns it
     * @param clientName The case-sensititive name of the client whose balance should be read
     * @return The balance of the client
     */
    Q_INVOKABLE QCA::BigInteger clientBalance(QString clientName);

    /**
     * @brief Updates the specified client's balance by adding the provided adjustment
     * @param clientName The case-sensitive name of the client whose balance should be adjusted
     * @param adjustment The amount to add to the balance (may be negative)
     */
    Q_INVOKABLE void adjustClientBalance(QString clientName, QCA::BigInteger adjustment);

    /**
     * @brief Decrypts the provided message
     * @param crypticMessage Encrypted message to decrypt
     * @return The plaintext of crypticMessage
     */
    QByteArray decryptMessage(QByteArray crypticMessage);
    /**
     * @brief Signs the provided message
     * @param legitMessage Totally trustworthy message to sign
     * @return The signature of the utterly legit message
     */
    QByteArray signMessage(QByteArray legitMessage);

signals:

protected slots:
    void clientConnecting();

protected:
    QTcpServer serverSocket;

    QCA::PrivateKey bankKey;

    /**
     * @brief initializeDatabase creates the initial static database of users and balances
     */
    void initializeDatabase();

    /**
     * @brief loadKey loads the bank's private key into RAM
     */
    void loadKey();

private:
    //NOTE: We represent balances as BigIntegers, to prevent overflows. This means that balance is stored as a count of cents rather than dollars.
    QMap<QString, QCA::BigInteger> balanceMap;
    QList<QString> lockedClients;
};

#endif // BANKCONTROLLER_H
