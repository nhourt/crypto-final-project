#ifndef CONSOLEINTERFACE_H
#define CONSOLEINTERFACE_H

#include "bankcontroller.h"

#include <QRunnable>
#include <QTextStream>

/**
 * @brief The ConsoleInterface class implements a simple CLI interface for bank employees to use to view
 * client balances, etc.
 */
class ConsoleInterface : public QRunnable
{
public:
    explicit ConsoleInterface();

    virtual void run();

    QString getCommand();

    void processCommand(QString command, QStringList arguments);

    bool lockClient(QString clientName);
    void unlockClient(QString clientName);

    void queryBalance(QString clientName);
    bool clientExists(QString clientName);
    void clientBalance(QString clientName, QCA::BigInteger balance);
    QCA::BigInteger clientBalance(QString clientName);
    QCA::BigInteger parseAmount(QString stringAmount);
    void depositToAccount(QString stringDeposit, QString clientName);
protected:
    QTextStream qin;
    QTextStream qout;
};

#endif // CONSOLEINTERFACE_H
