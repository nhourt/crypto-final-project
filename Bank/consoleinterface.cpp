#include "consoleinterface.h"

#include <cstdio>

ConsoleInterface::ConsoleInterface() :
    qin(stdin, QIODevice::ReadOnly),
    qout(stdout, QIODevice::WriteOnly)
{
}

void ConsoleInterface::run()
{
    while (1) {
        QString command = getCommand();

        QStringList arguments = command.split(' ', QString::SkipEmptyParts);
        command = arguments[0];
        arguments.removeFirst();

        processCommand(command, arguments);
    }
}

QString ConsoleInterface::getCommand()
{
    QString command;

    while (command.isEmpty()) {
        qout << "bank> " << flush;
        command = qin.readLine();
    }

    return command;
}

bool ConsoleInterface::clientExists(QString clientName)
{
    bool exists = false;

    QMetaObject::invokeMethod(BankController::globalInstance(), "clientExists", Qt::BlockingQueuedConnection, Q_RETURN_ARG(bool, exists), Q_ARG(QString, clientName));

    return exists;
}

QCA::BigInteger ConsoleInterface::clientBalance(QString clientName)
{
    QCA::BigInteger balance;

    QMetaObject::invokeMethod(BankController::globalInstance(), "clientBalance", Qt::BlockingQueuedConnection, Q_RETURN_ARG(QCA::BigInteger, balance), Q_ARG(QString, clientName));

    return balance;
}

void ConsoleInterface::queryBalance(QString clientName)
{
    if (!clientExists(clientName)){
        qout << "No such client: " << clientName << endl;
        return;
    }

    if (lockClient(clientName)) {
        QCA::BigInteger balance = clientBalance(clientName);
        qout << "Client " << clientName << "'s balance is " << BankController::printableBalance(balance) << endl;
        unlockClient(clientName);
    } else
        qout << "Cannot query account balance, as this client is currently logged into an ATM." << endl;
}

QCA::BigInteger ConsoleInterface::parseAmount(QString stringAmount)
{
    QStringList monies = stringAmount.split('.');
    QCA::BigInteger depositAmount = monies[0];
    depositAmount *= 100;
    if (monies.length() > 1)
        depositAmount += monies[1];
    return depositAmount;
}

void ConsoleInterface::depositToAccount(QString stringDeposit, QString clientName)
{
    if (!QRegExp("^\\d*(\\.\\d{2})?$").exactMatch(stringDeposit)) {
        qout << "Invalid deposit amount." << endl;
    }

    QCA::BigInteger depositAmount = parseAmount(stringDeposit);
    qDebug() << "Attempting to deposit" << BankController::printableBalance(depositAmount) << "to client" << clientName;

    if (!clientExists(clientName)) {
        qout << "No such client: " << clientName << endl;
    }
    if (depositAmount <= 0) {
        qout << "Cannot deposit nonpositive amount." << endl;
    }

    if (lockClient(clientName)) {
        QMetaObject::invokeMethod(BankController::globalInstance(), "adjustClientBalance", Qt::BlockingQueuedConnection, Q_ARG(QString, clientName), Q_ARG(QCA::BigInteger, depositAmount));
        unlockClient(clientName);
    } else
        qout << "Cannot deposit to client account, as this client is currently logged into an ATM." << endl;
}

void ConsoleInterface::processCommand(QString command, QStringList arguments)
{
    if (command == "balance" && arguments.length() == 1)
        queryBalance(arguments[0]);
    else if (command == "deposit" && arguments.length() == 2)
        depositToAccount(arguments[1], arguments[0]);
    else
        qout << "Invalid command or arguments." << endl;
}

bool ConsoleInterface::lockClient(QString clientName)
{
    bool success = false;

    QMetaObject::invokeMethod(BankController::globalInstance(), "lockClient", Qt::BlockingQueuedConnection, Q_RETURN_ARG(bool, success), Q_ARG(QString, clientName));

    return success;
}

void ConsoleInterface::unlockClient(QString clientName)
{
    QMetaObject::invokeMethod(BankController::globalInstance(), "unlockClient", Qt::BlockingQueuedConnection, Q_ARG(QString, clientName));
}
