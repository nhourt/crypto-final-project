#include "atminterface.h"
#include "bankcontroller.h"

#include <QCoreApplication>
#include <QStringList>

BankController *BankController::instance = NULL;
QMutex BankController::instanceMutex;

BankController::BankController(QObject *parent) :
    QObject(parent)
{
    loadKey();
    initializeDatabase();

    qDebug() << "Listening for connections on port" << qApp->arguments()[1].toInt();
    connect(&serverSocket, SIGNAL(newConnection()), this, SLOT(clientConnecting()));
    serverSocket.listen(QHostAddress::LocalHost, qApp->arguments()[1].toInt());
}

bool BankController::clientExists(QString clientName)
{
    return balanceMap.contains(clientName);
}

bool BankController::lockClient(QString clientName)
{
    if (lockedClients.contains(clientName))
        return false;

    qDebug() << "Locking client" << clientName;
    lockedClients.append(clientName);
    return true;
}

void BankController::unlockClient(QString clientName)
{
    if (lockedClients.contains(clientName)) {
        qDebug() << "Unlocking client" << clientName;
        lockedClients.removeAll(clientName);
    }
}

QCA::BigInteger BankController::clientBalance(QString clientName)
{
    if (clientExists(clientName))
        return balanceMap[clientName];
    return QCA::BigInteger(0);
}

void BankController::adjustClientBalance(QString clientName, QCA::BigInteger adjustment)
{
    if (!clientExists(clientName))
        return;

    adjustment += balanceMap[clientName];

    if (adjustment < 0) {
        qWarning() << "Illegal attempt to set balance to negative value not permitted.";
        return;
    }

    qDebug() << "Updating balance of client" << clientName << "to" << printableBalance(adjustment);
    balanceMap[clientName] = adjustment;
}

QByteArray BankController::decryptMessage(QByteArray crypticMessage)
{
    QCA::SecureArray plaintext;
    bankKey.decrypt(crypticMessage, &plaintext, QCA::EME_PKCS1_OAEP);
    return plaintext.toByteArray();
}

QByteArray BankController::signMessage(QByteArray legitMessage)
{
    return bankKey.signMessage(legitMessage, QCA::EMSA3_SHA1);
}

void BankController::clientConnecting()
{
    QTcpSocket *atmSocket = serverSocket.nextPendingConnection();
    qDebug() << "Received connection from ATM" << atmSocket->socketDescriptor();
    new AtmInterface(atmSocket, this);
}

void BankController::initializeDatabase()
{
    balanceMap["Alice"] = QCA::BigInteger(10000);
    balanceMap["Bob"] = QCA::BigInteger(5000);
    balanceMap["Eve"] = QCA::BigInteger(0);

    qDebug() << "Initialized database:";
    foreach (QString name, balanceMap.keys()) {
        qDebug() << "User:" << name << "Balance:" << printableBalance(balanceMap[name]);
    }
}

void BankController::loadKey()
{
    QCA::ConvertResult result;
    bankKey = QCA::PrivateKey::fromPEMFile("bank.privKey", "Now I am become Death, the destroyer of worlds.", &result);

    if (result != QCA::ConvertGood)
        qFatal("Failed to load private key. Cannot continue. Error code %d", result);
}
