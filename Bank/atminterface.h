#ifndef ATMINTERFACE_H
#define ATMINTERFACE_H

#include <QObject>
#include <QtCrypto>
#include <QNoDebug>
#include <QTcpSocket>

/**
 * @brief The AtmInterface class takes a TCP socket connected to the ATM and handles all requests from
 * a single ATM. One instance of this class must be created for each connecting ATM.
 */
class AtmInterface : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Creates a new AtmInterface managing the ATM connected on atmSocket.
     * @param atmSocket TCP socket connected to the ATM. AtmInterface will take ownership of this socket.
     * @param parent
     */
    explicit AtmInterface(QTcpSocket *atmSocket, QObject *parent = 0);
    virtual ~AtmInterface();
signals:

public slots:

protected slots:
    void incomingPacket();
    void closingConnection();
    void socketError();

protected:
    //We leave one byte for the cipher's padding
    const static int PACKET_SIZE = 1023;
    QTcpSocket *atmSocket;
    int connectionId;

    enum ConnectionState {StateUnauthenticated, StateAuthenticated};
    ConnectionState currentState;
    QString authenticatedUser;

    QCA::PublicKey atmKey;
    QCA::PublicKey userKey;

    QCA::SymmetricKey sessionKey;
    QCA::InitializationVector sessionIV;
    QCA::SecureArray lastNonce;
    QCA::Cipher aes;

    const static int IV_SIZE = 32;
    const static int SIGNATURE_SIZE = 512;
    const static int VALID_TIMESTAMP_WINDOW_SECONDS = 3;

    void decodePacket(QByteArray &packet);
    void processPacket(QByteArray packet);

    void authenticateUser(QByteArray plainText);

    void sendMessageToATM(QByteArray message);


    QByteArray encrypt(QByteArray message);
    QByteArray decrypt(QByteArray message);

    void sendBalanceToAtm();
    void processWithdrawal(QByteArray amount);
    void processTransfer(QByteArray amount, QString username);

    bool timestampOutOfWindow(QString stamp);
    bool validNonce(QByteArray suspectNonce);
    QCA::SecureArray getNonce();
};

#endif // ATMINTERFACE_H
