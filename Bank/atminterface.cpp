#include "atminterface.h"
#include "bankcontroller.h"

#include <QFileInfo>

AtmInterface::AtmInterface(QTcpSocket *atmSocket, QObject *parent):
    QObject(parent),
    atmSocket(atmSocket),
    connectionId(atmSocket->socketDescriptor()),
    currentState(StateUnauthenticated),
    aes("aes256", QCA::Cipher::CBC)
{
    atmSocket->setParent(this);
    connect(atmSocket, SIGNAL(readyRead()), this, SLOT(incomingPacket()));
    connect(atmSocket, SIGNAL(disconnected()), this, SLOT(closingConnection()));
    connect(atmSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(socketError()));
}

AtmInterface::~AtmInterface()
{
    qDebug() << "ATM connection ID" << connectionId << "is being deallocated.";

    if (authenticatedUser.length())
        BankController::globalInstance()->unlockClient(authenticatedUser);
}

void AtmInterface::incomingPacket()
{
    qDebug() << "Packet received from ATM, connection ID" << connectionId;
    QByteArray packet(atmSocket->readAll());
    processPacket(packet);
}

void AtmInterface::closingConnection()
{
    qDebug() << "ATM connection ID" << connectionId << "disconnecting.";
    atmSocket->close();
    deleteLater();
}

void AtmInterface::socketError()
{
    //The client disconnecting is not actually an error.
    if (atmSocket->error() == QAbstractSocket::RemoteHostClosedError)
        return;

    qWarning() << "Socket error on ATM connection ID" << connectionId << endl << atmSocket->errorString();
}

void AtmInterface::decodePacket(QByteArray &packet)
{
    packet = decrypt(packet);
    packet = packet.mid(sizeof(packet.size()), *(int*)packet.data());
}

void AtmInterface::processPacket(QByteArray packet)
{
    if (currentState == StateUnauthenticated) {
        authenticateUser(packet);
        return;
    }
    else if (currentState == StateAuthenticated) {
        //Need to decrypt the packet to determine what to do next.
        decodePacket(packet);
        qDebug() << "Got message from ATM:" << packet;

        QList<QByteArray> fields = packet.split(' ');

        //Check nonce first. If packet was invalid ciphertext or a replay, we'll bug out here.
        if(!validNonce(QByteArray::fromHex(fields[0]))) {
            qDebug() << "Rejecting packet: invalid nonce";
            atmSocket->write("wat");
            return;
        }

        if(fields[1] == "balance" && fields.size() == 2) {
            sendBalanceToAtm();
        } else if(fields[1] == "withdraw" && fields.size() == 3) {
            processWithdrawal(fields[2]);
        } else if (fields[1] == "transfer" && fields.size() == 4) {
            processTransfer(fields[2], fields[3]);
        } else {
            atmSocket->write("wat");
        }
        return;
    }

}

void AtmInterface::sendBalanceToAtm() {
    QCA::BigInteger balance = BankController::globalInstance()->clientBalance(authenticatedUser);
    QByteArray message;
    QCA::SecureArray nonce = getNonce();

    QTextStream(&message) << nonce.toByteArray().toHex() << " " << BankController::globalInstance()->printableBalance(balance);
    sendMessageToATM(message);
}

void AtmInterface::processWithdrawal(QByteArray amount)
{
    QCA::BigInteger withdrawAmount(amount.data());
    QCA::BigInteger userBalance = BankController::globalInstance()->clientBalance(authenticatedUser);
    QByteArray message;
    QCA::SecureArray nonce = getNonce();

    if (withdrawAmount > 0 && withdrawAmount <= userBalance) {
        qDebug() << "Processing withdrawal for user" << authenticatedUser << "of" << BankController::globalInstance()->printableBalance(withdrawAmount);

        //Rather than duplicate the math locally, we have BankController process the adjustment and report the new balance to us. No room for error this way.
        withdrawAmount *= -1;
        BankController::globalInstance()->adjustClientBalance(authenticatedUser, withdrawAmount);
        userBalance = BankController::globalInstance()->clientBalance(authenticatedUser);

        QTextStream(&message) << nonce.toByteArray().toHex() << " accept " << BankController::globalInstance()->printableBalance(userBalance);
    } else {
        qDebug() << "Rejecting withdrawal for user" << authenticatedUser << "of" << BankController::globalInstance()->printableBalance(withdrawAmount);
        QTextStream(&message) << nonce.toByteArray().toHex() << " reject " << BankController::globalInstance()->printableBalance(userBalance);
    }

    sendMessageToATM(message);
}

void AtmInterface::processTransfer(QByteArray amount, QString username)
{
    QCA::BigInteger transferAmount(amount.data());
    QCA::BigInteger userBalance = BankController::globalInstance()->clientBalance(authenticatedUser);
    QByteArray message;
    QCA::SecureArray nonce = getNonce();

    if (!BankController::globalInstance()->clientExists(username)){
        qDebug() << "Rejecting transfer for user " << authenticatedUser << " to " << username << " of " << BankController::globalInstance()->printableBalance(transferAmount);
        QTextStream(&message) << nonce.toByteArray().toHex() << " reject " << BankController::globalInstance()->printableBalance(userBalance) << " usererror";
        sendMessageToATM(message);
        return;
    }

    if (transferAmount > 0 && transferAmount <= userBalance) {
        qDebug() << "Processing transfer from " << authenticatedUser << " to " << username << " of " << BankController::globalInstance()->printableBalance(transferAmount);

        transferAmount *= -1;
        BankController::globalInstance()->adjustClientBalance(authenticatedUser, transferAmount);
        userBalance = BankController::globalInstance()->clientBalance(authenticatedUser);
        transferAmount *= -1;
        BankController::globalInstance()->adjustClientBalance(username, transferAmount);

        QTextStream(&message) << nonce.toByteArray().toHex() << " accept " << BankController::globalInstance()->printableBalance(userBalance) << " " << username;
    } else {
        qDebug() << "Rejecting transfer for user " << authenticatedUser << "to" << username << " of " << BankController::globalInstance()->printableBalance(transferAmount);
        QTextStream(&message) << nonce.toByteArray().toHex() << " reject " << BankController::globalInstance()->printableBalance(userBalance) << " amounterror";
    }

    sendMessageToATM(message);
}

void AtmInterface::authenticateUser(QByteArray packet)
{
    qDebug() << "Parsing packet of length" << packet.length();
    QByteArray signature = packet.right(SIGNATURE_SIZE);
    packet.truncate(SIGNATURE_SIZE);

    QByteArray plainText = BankController::globalInstance()->decryptMessage(packet);
    QList<QByteArray> subCommands = plainText.split(' ');

    qDebug() << "Size of subCommands: " << subCommands.size();
    qDebug() << "Packet: " << plainText;
    //Here we do initial validity checks on the incoming packet. We verify that it:
    // Has 4 components
    // The ATM name is alphanumeric/hyphenated
    // We have the ATM's public key
    // The client name is alphanumeric/hyphenated
    // The client exists in the database
    // We have the client's public key
    // We have the client's passphrase
    // The timestamp is within legal range
    // .... whew!
    if (
            subCommands.size() != 4 ||
            !QRegExp("^[a-zA-Z0-9\\-]+$").exactMatch(subCommands[0]) ||
            !QFileInfo("atm_keys/" + subCommands[0] + ".pubKey").exists() ||
            !QRegExp("^[a-zA-Z0-9\\-]+$").exactMatch(subCommands[1]) ||
            !BankController::globalInstance()->clientExists(subCommands[1]) ||
            !QFileInfo("user_keys/" + subCommands[1] + ".pubKey").exists() ||
            !QFileInfo("user_keys/" + subCommands[1] + ".pass").exists() ||
            timestampOutOfWindow(subCommands[2])) {
        atmSocket->write("wat");
        return;
    }

    QCA::ConvertResult result;
    //Open ATM key
    atmKey = QCA::PublicKey::fromPEMFile("atm_keys/" + subCommands[0] + ".pubKey", &result);

    //Finally get around to validating the signature...
    if (result != QCA::ConvertGood || !atmKey.verifyMessage(packet, signature, QCA::EMSA3_SHA1)) {
        atmSocket->write("wat");
        return;
    }

    //Open client key
    userKey = QCA::PublicKey::fromPEMFile("user_keys/" + subCommands[1] + ".pubKey", &result);
    if (result != QCA::ConvertGood) {
        atmSocket->write("wat");
        return;
    }

    //Verify that the user isn't logged in elsewhere
    if (!BankController::globalInstance()->lockClient(subCommands[1])) {
        qDebug() << "Blocking parallel login of user" << subCommands[1];
        atmSocket->write("wat");
        return;
    }

    qDebug() << "Authenticating user" << subCommands[1];

    //Generate random session key and IV, encrypt them with user's public key
    sessionKey = QCA::SymmetricKey(IV_SIZE);
    sessionIV = QCA::InitializationVector(IV_SIZE);
    QCA::SecureArray sessionPackage = sessionKey;
    sessionPackage.append(sessionIV);
    sessionPackage.append(QCA::Hash("sha256").hash(QByteArray::fromHex(subCommands[3])));
    sessionPackage = userKey.encrypt(sessionPackage, QCA::EME_PKCS1_OAEP);

    //Open client's card passphrase
    QFile passFile("user_keys/" + subCommands[1] + ".pass");
    passFile.open(QIODevice::ReadOnly);
    QByteArray userPass = passFile.readAll();
    passFile.close();

    packet.clear();
    //Calculate the next nonce and build the packet
    lastNonce = QCA::Hash("sha256").hash(QByteArray::fromHex(subCommands[3]));
    QTextStream(&packet) << userPass.toHex() << " " << lastNonce.toByteArray().toHex();

    //Encrypt the packet and ship it out
    packet = atmKey.encrypt(packet, QCA::EME_PKCS1_OAEP).toByteArray();
    packet.append(sessionPackage.toByteArray());
    atmSocket->write(packet);

    //State transition to authenticated mode. If the user can read the session key, he's legit.
    //We will pedantically ignore *all* subsequent packets that are not encrypted with that session key.
    currentState = StateAuthenticated;
    authenticatedUser = subCommands[1];
}

QByteArray AtmInterface::encrypt(QByteArray message)
{
    aes.setup(QCA::Encode, sessionKey, sessionIV);
    return aes.process(message).toByteArray();
}

QByteArray AtmInterface::decrypt(QByteArray message)
{
    aes.setup(QCA::Decode, sessionKey, sessionIV);
    return aes.process(message).toByteArray();
}

bool AtmInterface::timestampOutOfWindow(QString stamp)
{
    return qAbs(stamp.toInt() - QDateTime::currentDateTime().toTime_t()) > VALID_TIMESTAMP_WINDOW_SECONDS;
}

bool AtmInterface::validNonce(QByteArray suspectNonce) {
    if (QCA::Hash("sha256").hash(lastNonce).toByteArray() == suspectNonce) {
        lastNonce = QCA::Hash("sha256").hash(lastNonce).toByteArray();
        return true;
    }
    return false;
}

QCA::SecureArray AtmInterface::getNonce() {
//Generate a new nonce from the lastNonce, store it, and return it.
    QCA::SecureArray newNonce = QCA::Hash("sha256").hash(lastNonce);
    lastNonce = newNonce;
    return newNonce;
}

void AtmInterface::sendMessageToATM(QByteArray message)
{
    qDebug() << "Sending message to ATM:" << message;
    int rawSize = message.size();
    message.prepend((char*)&rawSize, sizeof(rawSize));
    if (message.size() < PACKET_SIZE)
        message.resize(PACKET_SIZE);

    atmSocket->write(encrypt(message));
}
